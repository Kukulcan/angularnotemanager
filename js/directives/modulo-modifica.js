//controller associato alla direttiva sottostante
angular.module("NoteManager")
        .controller("modificaController",["$scope",function($scope){
                
            //scope isolato del controller
    
            //variabile booleana per controllare se il pulsante "Nuova" è stato cliccato.
            $scope.check=false;
            $scope.nuova = function(){
                //trovare l'id dell'ultima nota nell'elenco delle note
                //diversi modi per farlo, tra cui $scope.elencoNote.pop() oppure scorrere tutti gli elementi
                //di $scope.elencoNote, creare un array contenente tutti i valori degli id, trovare il più alto, aumentarlo di 1.
                var lastId = $scope.elencoNote[$scope.elencoNote.length-1].id;
                
                //aumento di 1 il valore trovato.
                lastId++;
                $scope.nota = {
                    id: lastId,
                    gruppo:"To Do"
                };
                $scope.check=true;
            }
            
            $scope.salva=function(){
                $scope.elencoNote.push($scope.nota);
                $scope.nota = {};
                $scope.check = false;
            }
        }]);

//direttiva modulo-modifica , ha uno scope isolato
app.directive('moduloModifica', function() {
    return {
	restrict: 'E',
        scope: {
            //variabili visibili all'interno dello scope isolato.
            // nota - elencoGruppi - elencoNote. prendono i valori dagli attributi dell'elemento html modulo-modifica
            //a sinistra; il nome della variabile JS all'interno dello scope, a destra l'attributo html correlato.
            nota:'=',
            elencoGruppi:'=gruppi',
            elencoNote:'=note'
        },
        templateUrl:'js/directives/modulo-modifica.html',
        //la direttiva ha un suo controller associato.  Definito in questo stesso file in quanto utilizzato solo da questo.
        controller:"modificaController"
    };
});