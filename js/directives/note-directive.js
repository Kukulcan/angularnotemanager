//direttiva personalizzata single-note , non ha scope isolato. Ha lo scope di visibilità del controller in cui è posta.
angular.module("NoteManager")
        .directive("singleNote",function(){
            return {
              restrict: "EA",
              templateUrl: "js/directives/note-directive.html"
            };
        });


