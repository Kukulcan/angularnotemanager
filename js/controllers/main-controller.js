angular.module("NoteManager")
        .controller("mainController",["$scope","getNotes",function($scope,getNotes){
             $scope.notaCliccata = {};
             getNotes.then(
                //funzione di successo della chiamata http
                function(risposta){
                    //valorizzo 2 vettori con dati provenienti da un file json restituito dalla chiamata http
                    $scope.elencoNote = risposta.data.note;
                    $scope.elencoGruppi = risposta.data.gruppi;
                                     
                }
             );
             
             //funzione aggiungiNota : associa ad una oggetto dello scope, la nota cliccata.
             //la nota viene passata interamente come argomento della funzione associata al click.
             $scope.aggiungiNota = function(nota){
                 $scope.notaCliccata = nota;
             }
        }]);

